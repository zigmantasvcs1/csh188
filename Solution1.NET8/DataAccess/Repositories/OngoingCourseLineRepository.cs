﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
	public class OngoingCourseLineRepository // Sukuriama OngoingCourseLineRepository klasė
	{
		private readonly CoursesDbContext _context; // Privatus readonly laukas _context

		public OngoingCourseLineRepository(CoursesDbContext context) // Konstruktorius
		{
			_context = context;
		}

		// Create
		public async Task CreateAsync(OngoingCourseLine ongoingCourseLine) // Asinchroninis metodas įrašyti naują eilutę
		{
			_context.OngoingCourseLines.Add(ongoingCourseLine); // Pridedama nauja eilutė į OngoingCourseLines kolekciją

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Read
		public async Task<OngoingCourseLine?> ReadAsync(int id) // Asinchroninis metodas gauti eilutę pagal ID
		{
			return await _context.OngoingCourseLines
				.Include(ocl => ocl.OngoingCourse)
				.Include(ocl => ocl.Student)
				.Include(ocl => ocl.Lecturer)
				.FirstOrDefaultAsync(ocl => ocl.Id == id); // Grąžinama eilutė pagal ID arba null
		}

		public async Task<List<OngoingCourseLine>> ReadAsync() // Asinchroninis metodas gauti visas eilutes
		{
			return await _context.OngoingCourseLines
				.Include(ocl => ocl.OngoingCourse)
				.Include(ocl => ocl.Student)
				.Include(ocl => ocl.Lecturer)
				.ToListAsync(); // Grąžinamas sąrašas
		}

		// Update
		public async Task UpdateAsync(OngoingCourseLine ongoingCourseLine) // Asinchroninis metodas atnaujinti eilutę
		{
			var existingLine = await _context.OngoingCourseLines.FindAsync(ongoingCourseLine.Id); // Ieškoma eilutė

			if (existingLine == null) // Jei nerasta, išmetama išimtis
			{
				throw new KeyNotFoundException($"OngoingCourseLine with ID {ongoingCourseLine.Id} not found.");
			}

			_context.Entry(existingLine).CurrentValues.SetValues(ongoingCourseLine); // Atnaujinamos reikšmės

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai
		}

		// Delete
		public async Task DeleteAsync(int id) // Asinchroninis metodas ištrinti eilutę
		{
			var line = await _context.OngoingCourseLines.FindAsync(id); // Ieškoma eilutė

			if (line == null) // Jei nerasta, išmetama išimtis
			{
				throw new KeyNotFoundException($"OngoingCourseLine with ID {id} not found.");
			}

			_context.OngoingCourseLines.Remove(line); // Pašalinama eilutė

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai
		}
	}
}
