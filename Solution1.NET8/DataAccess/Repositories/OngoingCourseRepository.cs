﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
	public class OngoingCourseRepository // Sukuriama OngoingCourseRepository klasė, skirta valdyti vykstančių kursų informaciją duomenų bazėje
	{
		private readonly CoursesDbContext _context; // Privatus readonly laukas _context, naudojamas bendrauti su duomenų baze

		public OngoingCourseRepository(CoursesDbContext context) // Konstruktorius, kuris priima CoursesDbContext tipo objektą ir priskiria jį _context laukui
		{
			_context = context;
		}

		// CRUD operacijos

		// Create
		public async Task CreateAsync(OngoingCourse ongoingCourse) // Asinchroninis metodas CreateAsync, skirtas įrašyti naują vykstantį kursą į duomenų bazę
		{
			_context.OngoingCourses.Add(ongoingCourse); // Pridedamas vykstantis kursas į OngoingCourses kolekciją

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Read
		public async Task<OngoingCourse?> ReadAsync(int id) // Asinchroninis metodas ReadAsync, skirtas gauti vykstantį kursą pagal jo ID
		{
			return await _context
				.OngoingCourses
				.Include(oc => oc.Course) // Užtikrinama, kad būtų įkeltas ir Course objektas
				.FirstOrDefaultAsync(oc => oc.Id == id); // Grąžinamas pirmas rastas vykstantis kursas pagal ID arba null, jei tokio nėra
		}

		public async Task<List<OngoingCourse>> ReadAsync() // Asinchroninis metodas ReadAsync, skirtas gauti visus vykstančius kursus
		{
			return await _context
				.OngoingCourses
				.Include(oc => oc.Course) // Užtikrinama, kad būtų įkelti ir Course objektai
				.ToListAsync(); // Grąžinamas visų vykstančių kursų sąrašas
		}

		// Update
		public async Task UpdateAsync(OngoingCourse ongoingCourse) // Asinchroninis metodas UpdateAsync, skirtas atnaujinti esamo vykstančio kurso informaciją
		{
			var existingOngoingCourse = await _context
				.OngoingCourses
				.FindAsync(ongoingCourse.Id); // Ieškomas vykstantis kursas pagal ID

			if (existingOngoingCourse == null) // Jei vykstantis kursas nerastas, išmetama išimtis
			{
				throw new KeyNotFoundException($"OngoingCourse with ID {ongoingCourse.Id} not found.");
			}

			_context.Entry(existingOngoingCourse).CurrentValues.SetValues(ongoingCourse); // Atnaujinamos esamo vykstančio kurso reikšmės

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Delete
		public async Task DeleteAsync(int id) // Asinchroninis metodas DeleteAsync, skirtas ištrinti vykstantį kursą pagal ID
		{
			var ongoingCourse = await _context
				.OngoingCourses
				.FindAsync(id); // Ieškomas vykstantis kursas pagal ID

			if (ongoingCourse == null) // Jei vykstantis kursas nerastas, išmetama išimtis
			{
				throw new KeyNotFoundException($"OngoingCourse with ID {id} not found.");
			}

			_context
				.OngoingCourses
				.Remove(ongoingCourse); // Pašalinamas vykstantis kursas iš OngoingCourses kolekcijos

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}
	}
}
