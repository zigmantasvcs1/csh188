﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
	public class Course : BaseEntity
	{
		public Course(
			string title,
			decimal price,
			int hours,
			string description,
			int id,
			DateTime createdAt) 
			: base(
				  id, 
				  createdAt
			)
		{
			Title = title;
			Price = price;
			Hours = hours;
			Description = description;
		}

		[Required]
		[MaxLength(50)]
		public string Title { get; private set; } = null!;

		[Column(TypeName = "decimal(7, 2)")]
		public decimal Price { get; private set; }

		public int Hours { get; private set; }

		[Required]
		[MaxLength(250)]
		public string Description { get; private set; } = null!;
	}
}
