﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
	public class OngoingCourseLine : BaseEntity
	{
		private OngoingCourseLine() { }

		public OngoingCourseLine(
			int ongoingCourseId,
			int lecturerId,
			int studentId,
			int id,
			DateTime createdAt) 
			: base(
				  id,
				  createdAt)
		{
			OngoingCourseId = ongoingCourseId;
			LecturerId = lecturerId;
			StudentId = studentId;
		}

		public int OngoingCourseId { get; private set; }
		public OngoingCourse OngoingCourse { get; set; } // Navigacinė savybė

		public int StudentId { get; private set; }
		public Student Student { get; set; } // Navigacinė savybė

		public int LecturerId { get; private set; }
		public Lecturer Lecturer { get; set; } // Navigacinė savybė
	}
}
