﻿using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
	public class BaseEntity
	{
		protected BaseEntity() { }

		public BaseEntity(
			int id,
			DateTime createdAt)
		{
			if(id != 0)
			{
				Id = id;
			}

			if(createdAt != DateTime.MinValue)
			{
				CreatedAt = createdAt;
			}
		}

		public int Id { get; private set; }

		[Required]
		public DateTime CreatedAt { get; private set; } = DateTime.Now;
	}
}
