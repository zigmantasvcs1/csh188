﻿namespace DataAccess.Entities
{
	public class Student : CoursePerson
	{
		public Student(
			string name,
			string surname,
			string email,
			string documentNumber,
			DateTime? birthDay,
			int id,
			DateTime createdAt) 
			: base(
				  name,
				  surname,
				  email,
				  documentNumber,
				  id,
				  createdAt)
		{
			BirthDay = birthDay;
		}

		public DateTime? BirthDay { get; private set; }
	}
}
