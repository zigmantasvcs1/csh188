﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Newtonsoft.Json;

namespace CoursesApplication.Handlers
{
	public class OngoingCoursesHandler
	{
		private readonly OngoingCourseRepository _ongoingCourseRepository;

		public OngoingCoursesHandler(OngoingCourseRepository ongoingCourseRepository)
		{
			_ongoingCourseRepository = ongoingCourseRepository;
		}

		public async Task HandleAsync()
		{
			var continueKey = "t";
			do
			{
				Console.WriteLine("Patekote į vykstančių kursų aplinką");
				Console.WriteLine("Ką norite daryti");
				Console.WriteLine("1 - eksportuoti vykstančius kursus į JSON failą");
				Console.WriteLine("2 - išspausdinti vykstančius kursus ekrane");
				Console.WriteLine("3 - sukurti vykstančius kursą");
				Console.WriteLine("4 - pakeisti vykstančius kursą");
				Console.WriteLine("5 - ištrinti vykstančius kursą");

				Console.WriteLine("Įveskite ką renkatės");
				switch (Console.ReadLine())
				{
					case "1":
						await ExportAsync();
						break;
					case "2":
						await PrintAsync();
						break;
					case "3":
						await CreateAsync();
						break;
					case "4":
						await ChangeAsync();
						break;
					case "5":
						await DeleteAsync();
						break;
					default:
						Console.WriteLine("Nėra tokio pasirinkimo");
						break;
				}

				Console.WriteLine("Ar tęsiame? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private async Task ExportAsync()
		{
			List<OngoingCourse> ongoingCourses = await _ongoingCourseRepository.ReadAsync();

			var jsString = JsonConvert.SerializeObject(ongoingCourses);

			ExportToJson(jsString, "ongoingCourses.json");
			Console.WriteLine("Failas ongoingCourses.json exportuotas");
		}

		private void ExportToJson(string json, string fileName)
		{
			using (var streamWriter = new StreamWriter(fileName))
			{
				streamWriter.WriteLine(json);
			}
		}

		private async Task PrintAsync()
		{
			List<OngoingCourse> ongoingCourses = await _ongoingCourseRepository.ReadAsync();

			Console.WriteLine("Vykstantys kursai:");
			Console.WriteLine($"" +
				$"{"Id",-5} " +
				$"{"Antraštė",-20} " +
				$"{"Pradžia",-20} " +
				$"{"Pabaiga",-25} " +
				$"{"Sukurta",-25}"
			);

			foreach (var ongoingCourse in ongoingCourses)
			{
				Console.WriteLine($"" +
					$"{ongoingCourse.Id,-5} " +
					$"{ongoingCourse.Course?.Title,-20} " +
					$"{ongoingCourse.StartDate:yyyy-MM-dd,-20} " +
					$"{ResolveDateTimeString(ongoingCourse),-25} " +
					$"{ongoingCourse.CreatedAt:yyyy-MM-dd,-15}");
			}
		}

		private string ResolveDateTimeString(OngoingCourse ongoingCourse)
		{
			return ongoingCourse.EndDate.HasValue
				? ongoingCourse.EndDate.Value.ToString("yyyy-MM-dd")
				: string.Empty;
		}

		private async Task CreateAsync()
		{
			Console.WriteLine("Suveskite vykstančio kurso duomenis");

			Console.WriteLine("Įveskite kurso id ");
			int courseId = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite pradžios datą");
			DateTime startDate = DateTime.Parse(Console.ReadLine());

			DateTime? endDate = null;
			Console.WriteLine("Įveskite pabaigos datą");
			if (DateTime.TryParse(Console.ReadLine(), out DateTime parsedDateTime))
			{
				endDate = parsedDateTime;
			}

			OngoingCourse ongoingCourse = new OngoingCourse(
				courseId,
				startDate,
				endDate,
				0,
				DateTime.MinValue
			);

			await _ongoingCourseRepository.CreateAsync(ongoingCourse);

			Console.WriteLine("Vykstantis kursas sukurtas");
		}

		private async Task ChangeAsync()
		{
			Console.WriteLine("Suveskite vykstnačio kurso duomenis, kuriuos keisite");

			Console.WriteLine("Kokį vysktantį kursą keisite? Pasirinkite id iš sąrašo.");
			await PrintAsync();

			Console.WriteLine("Įveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite kurso id ");
			int courseId = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite pradžios datą");
			DateTime startDate = DateTime.Parse(Console.ReadLine());

			DateTime? endDate = null;
			Console.WriteLine("Įveskite pabaigos datą");
			if (DateTime.TryParse(Console.ReadLine(), out DateTime parsedDateTime))
			{
				endDate = parsedDateTime;
			}

			OngoingCourse ongoingCourse = new OngoingCourse(
				courseId,
				startDate,
				endDate,
				id,
				DateTime.MinValue
			);

			await _ongoingCourseRepository.UpdateAsync(ongoingCourse);

			Console.WriteLine("Vysktnatis kursas koreguotas");
		}

		private async Task DeleteAsync()
		{
			Console.WriteLine("Kokį vysktantį kursą trinsite?");

			await PrintAsync();

			Console.WriteLine("Įveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Ar tikrai to norite? t/n");

			if (Console.ReadLine() == "t")
			{
				await _ongoingCourseRepository.DeleteAsync(id);

				Console.WriteLine($"Vykstantis kursas ištrintas");
			}
			else
			{
				Console.WriteLine("Vykstantis kursas nebuvo ištrintas");
			}
		}
	}
}
