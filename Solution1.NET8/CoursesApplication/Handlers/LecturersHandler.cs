﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Newtonsoft.Json;

namespace CoursesApplication.Handlers
{
	public class LecturersHandler
	{
		private readonly LecturerRepository _lecturerRepository;

		public LecturersHandler(LecturerRepository lecturerRepository)
		{
			_lecturerRepository = lecturerRepository;
		}

		public async Task HandleAsync()
		{
			var continueKey = "t";
			do
			{
				Console.WriteLine("Patekote į lektorių aplinką");
				Console.WriteLine("Ką norite daryti");
				Console.WriteLine("1 - eksportuoti lektorius į JSON failą");
				Console.WriteLine("2 - išspausdinti lektorius ekrane");
				Console.WriteLine("3 - sukurti lektorių");
				Console.WriteLine("4 - pakeisti lektorių");
				Console.WriteLine("5 - ištrinti lektorių");

				Console.WriteLine("Įveskite ką renkatės");
				switch (Console.ReadLine())
				{
					case "1":
						await ExportAsync();
						break;
					case "2":
						await PrintAsync();
						break;
					case "3":
						await CreateAsync();
						break;
					case "4":
						await ChangeAsync();
						break;
					case "5":
						await DeleteAsync();
						break;
					default:
						Console.WriteLine("Nėra tokio pasirinkimo");
						break;
				}

				Console.WriteLine("Ar tęsiame? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private async Task ExportAsync()
		{
			List<Lecturer> lecturers = await _lecturerRepository.ReadAsync();

			var jsString = JsonConvert.SerializeObject(lecturers);

			ExportToJson(jsString, "lecturers.json");
			Console.WriteLine("Failas lecturers.json exportuotas");
		}

		private void ExportToJson(string json, string fileName)
		{
			using (var streamWriter = new StreamWriter(fileName))
			{
				streamWriter.WriteLine(json);
			}
		}

		private async Task PrintAsync()
		{
			List<Lecturer> lecturers = await _lecturerRepository.ReadAsync();

			Console.WriteLine("Lecturers:");
			Console.WriteLine($"{"Id",-5} {"Vardas",-20} {"Pavardė",-20} {"El. paštas",-25} {"Dok. Nr.",-15}");

			foreach (var lecturer in lecturers)
			{
				Console.WriteLine($"{lecturer.Id,-5} {lecturer.Name,-20} {lecturer.Surname,-20} {lecturer.Email,-25} {lecturer.DocumentNumber,-15}");
			}
		}

		private async Task CreateAsync()
		{
			Console.WriteLine("Suveskite lektoriaus duomenis");

			Console.WriteLine("Įveskite vardą");
			string name = Console.ReadLine();

			Console.WriteLine("Įveskite pavardę");
			string surname = Console.ReadLine();

			Console.WriteLine("Įveskite el. paštą");
			string email = Console.ReadLine();

			Console.WriteLine("Įveskite dokumento numerį");
			string documentNumber = Console.ReadLine();

			Lecturer student = new Lecturer(
				name,
				surname,
				email,
				documentNumber,
				0,
				DateTime.MinValue
			);

			await _lecturerRepository.CreateAsync(student);

			Console.WriteLine("Lektorius sukurtas");
		}

		private async Task ChangeAsync()
		{
			Console.WriteLine("Suveskite lektoriaus duomenis, kuriuos keisite");

			Console.WriteLine("Kokį lektorių keisite? Pasirinkite id iš sąrašo.");
			await PrintAsync();

			Console.WriteLine("Įveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite vardą");
			string name = Console.ReadLine();

			Console.WriteLine("Įveskite pavardę");
			string surname = Console.ReadLine();

			Console.WriteLine("Įveskite el. paštą");
			string email = Console.ReadLine();

			Console.WriteLine("Įveskite dokumento numerį");
			string documentNumber = Console.ReadLine();

			Lecturer lecturer = new Lecturer(
				name,
				surname,
				email,
				documentNumber,
				id, // reikia id
				DateTime.MinValue
			);

			await _lecturerRepository.UpdateAsync(lecturer);

			Console.WriteLine("Lektorius koreguotas");
		}

		private async Task DeleteAsync()
		{
			Console.WriteLine("Kokį lektorių trinsite?");

			await PrintAsync();

			Console.WriteLine("Įveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Ar tikrai to norite? t/n");

			if (Console.ReadLine() == "t")
			{
				await _lecturerRepository.DeleteAsync(id);

				Console.WriteLine($"Lektorius ištrintas");
			}
			else
			{
				Console.WriteLine("Lektorius nebuvo ištrintas");
			}
		}
	}
}
