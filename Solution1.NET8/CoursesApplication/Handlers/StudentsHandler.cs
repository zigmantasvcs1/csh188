﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Newtonsoft.Json;

namespace CoursesApplication.Handlers
{
	public class StudentsHandler
	{
		private readonly StudentRepository _studentRepository;

		public StudentsHandler(StudentRepository studentRepository)
		{
			_studentRepository = studentRepository;
		}

		public async Task HandleAsync()
		{
			var continueKey = "t";
			do
			{
				Console.WriteLine("Patekote į studentų aplinką");
				Console.WriteLine("Ką norite daryti");
				Console.WriteLine("1 - eksportuoti studentus į JSON failą");
				Console.WriteLine("2 - išspausdinti studentus ekrane");
				Console.WriteLine("3 - sukurti studentą");
				Console.WriteLine("4 - pakeisti studentą");
				Console.WriteLine("5 - ištrinti studentą");

				Console.WriteLine("Įveskite ką renkatės");
				switch (Console.ReadLine())
				{
					case "1":
						await ExportAsync();
						break;
					case "2":
						await PrintAsync();
						break;
					case "3":
						await CreateAsync();
						break;
					case "4":
						await ChangeAsync();
						break;
					case "5":
						await DeleteAsync();
						break;
					default:
						Console.WriteLine("Nėra tokio pasirinkimo");
						break;
				}

				Console.WriteLine("Ar tęsiame? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private async Task ExportAsync()
		{
			List<Student> students = await _studentRepository.ReadAsync();

			var jsString = JsonConvert.SerializeObject(students);

			ExportToJson(jsString);
			Console.WriteLine("Failas students.json exportuotas");
		}

		private void ExportToJson(string json)
		{
			using (var streamWriter = new StreamWriter("students.json"))
			{
				streamWriter.WriteLine(json);
			}
		}

		private async Task PrintAsync()
		{
			// Gaunamas studentų sąrašas iš duomenų bazės
			List<Student> students = await _studentRepository.ReadAsync();

			Console.WriteLine("Students:");
			// Antraštės spausdinimas, nustatant stulpelių plotį
			Console.WriteLine($"" +
				$"{"Id",-5} " +
				$"{"Vardas",-20} " +
				$"{"Pavardė",-20} " +
				$"{"El. paštas",-25} " +
				$"{"Dok. Nr.",-15} " +
				$"{"Gimimo data",-10} " +
				$"{"Sukūrimo data",-10}"
			);
			
			foreach (var student in students)
			{
				// Kiekvieno studento duomenų spausdinimas, laikantis nustatyto stulpelių pločio
				Console.WriteLine($"" +
					$"{student.Id,-5} " +
					$"{student.Name,-20} " +
					$"{student.Surname,-20} " +
					$"{student.Email,-25} " +
					$"{student.DocumentNumber,-15} " +
					$"{ResolveDateTimeString(student),-10} " +
					$"{student.CreatedAt:yyyy-MM-dd,-10}"
				);
			}
		}

		private string ResolveDateTimeString(Student student)
		{
			return student.BirthDay.HasValue 
				? student.BirthDay.Value.ToString("yyyy-MM-dd") 
				: string.Empty;
		}

		private async Task CreateAsync()
		{
			Console.WriteLine("Suveskite studento duomenis");

			Console.WriteLine("Įveskite vardą");
			string name = Console.ReadLine();

			Console.WriteLine("Įveskite pavardę");
			string surname = Console.ReadLine();

			Console.WriteLine("Įveskite el. paštą");
			string email = Console.ReadLine();

			Console.WriteLine("Įveskite dokumento numerį");
			string documentNumber = Console.ReadLine();

			DateTime? birthDay = null;
			Console.WriteLine("Įveskite gimimo datą");
			if(DateTime.TryParse(Console.ReadLine(), out DateTime parsedDateTime))
			{
				birthDay = parsedDateTime;
			}

			Student student = new Student(
				name,
				surname,
				email,
				documentNumber,
				birthDay,
				0,
				DateTime.MinValue
			);

			await _studentRepository.CreateAsync(student);

			Console.WriteLine("Studentas sukurtas");
		}

		private async Task ChangeAsync()
		{
			Console.WriteLine("Suveskite studento duomenis, kuriuos keisite");

			Console.WriteLine("Kokį studentą keisite? Pasirinkite id iš sąrašo.");
			await PrintAsync();

			Console.WriteLine("Įveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite vardą");
			string name = Console.ReadLine();

			Console.WriteLine("Įveskite pavardę");
			string surname = Console.ReadLine();

			Console.WriteLine("Įveskite el. paštą");
			string email = Console.ReadLine();

			Console.WriteLine("Įveskite dokumento numerį");
			string documentNumber = Console.ReadLine();

			DateTime? birthDay = null;
			Console.WriteLine("Įveskite gimimo datą");
			if (DateTime.TryParse(Console.ReadLine(), out DateTime parsedDateTime))
			{
				birthDay = parsedDateTime;
			}

			Student student = new Student(
				name,
				surname,
				email,
				documentNumber,
				birthDay,
				id, // reikia id
				DateTime.MinValue
			);

			await _studentRepository.UpdateAsync(student);

			Console.WriteLine("Studentas koreguotas");
		}

		private async Task DeleteAsync()
		{
			Console.WriteLine("Kokį studentą trinsite? Pasirinkite id iš sąrašo.");
			await PrintAsync();

			Console.WriteLine("Įveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Ar tikrai to norite? t/n");

			if (Console.ReadLine() == "t")
			{
				await _studentRepository.DeleteAsync(id);

				Console.WriteLine($"Studentas ištrintas");
			}
			else
			{
				Console.WriteLine("Studentas nebuvo ištrintas");
			}
		}
	}
}
