﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoursesApplication.Handlers
{
	public class CoursesHandler
	{
		private readonly CourseRepository _courseRepository;

		public CoursesHandler(CourseRepository courseRepository)
		{
			_courseRepository = courseRepository;
		}

		public async Task HandleAsync()
		{
			var continueKey = "t";
			do
			{
				Console.WriteLine("Patekote į kursų aplinką");
				Console.WriteLine("Ką norite daryti");
				Console.WriteLine("1 - eksportuoti kursus į JSON failą");
				Console.WriteLine("2 - išspausdinti kursus ekrane");
				Console.WriteLine("3 - sukurti kursą");
				Console.WriteLine("4 - pakeisti kursą");
				Console.WriteLine("5 - ištrinti kursą");

				Console.WriteLine("Įveskite ką renkatės");
				switch (Console.ReadLine())
				{
					case "1":
						await ExportAsync();
						break;
					case "2":
						await PrintAsync();
						break;
					case "3":
						await CreateAsync();
						break;
					case "4":
						await ChangeAsync();
						break;
					case "5":
						await DeleteAsync();
						break;
					default:
						Console.WriteLine("Nėra tokio pasirinkimo");
						break;
				}

				Console.WriteLine("Ar tęsiame? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private async Task ExportAsync()
		{
			List<Course> courses = await _courseRepository.ReadAsync();

			var jsString = JsonConvert.SerializeObject(courses);

			ExportToJson(jsString, "courses.json");
			Console.WriteLine("Failas courses.json exportuotas");
		}

		private void ExportToJson(string json, string fileName)
		{
			using (var streamWriter = new StreamWriter(fileName))
			{
				streamWriter.WriteLine(json);
			}
		}

		private async Task PrintAsync()
		{
			List<Course> courses = await _courseRepository.ReadAsync();

			Console.WriteLine("Kursai:");
			Console.WriteLine($"{"Id",-5} {"Antraštė",-20} {"Trukmė",-20} {"Kaina",-25} {"Aprašymas",-50}");

			foreach (var course in courses)
			{
				Console.WriteLine($"{course.Id,-5} {course.Title,-20} {course.Hours,-20} {course.Price,-25} {course.Description,-15}");
			}
		}

		private async Task CreateAsync()
		{
			Console.WriteLine("Suveskite kurso duomenis");

			Console.WriteLine("Įveskite pavadinimą");
			string title = Console.ReadLine();

			Console.WriteLine("Įveskite kainą. Kableliais ar taškais atskirti skaičiai žiūrėkite pagal savo lokalizaciją");
			decimal price = decimal.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite valandas");
			int hours = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite aprašymą");
			string description = Console.ReadLine();

			Course course = new Course(
				title,
				price,
				hours,
				description,
				0,
				DateTime.MinValue
			);

			await _courseRepository.CreateAsync(course);

			Console.WriteLine("Kursas sukurtas");
		}

		private async Task ChangeAsync()
		{
			Console.WriteLine("Suveskite kurso duomenis, kuriuos keisite");

			Console.WriteLine("Kokį kursą keisite? Pasirinkite id iš sąrašo.");
			await PrintAsync();

			Console.WriteLine("Įveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite pavadinimą");
			string title = Console.ReadLine();

			Console.WriteLine("Įveskite kainą. Kableliais ar taškais atskirti skaičiai žiūrėkite pagal savo lokalizaciją");
			decimal price = decimal.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite valandas");
			int hours = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite aprašymą");
			string description = Console.ReadLine();

			Course course = new Course(
				title,
				price,
				hours,
				description,
				id, // reikia id
				DateTime.MinValue
			);

			await _courseRepository.UpdateAsync(course);

			Console.WriteLine("Kursas koreguotas");
		}

		private async Task DeleteAsync()
		{
			Console.WriteLine("Kokį kursą trinsite?");

			await PrintAsync();

			Console.WriteLine("Įveskite id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Ar tikrai to norite? t/n");

			if (Console.ReadLine() == "t")
			{
				await _courseRepository.DeleteAsync(id);

				Console.WriteLine($"Kursas ištrintas");
			}
			else
			{
				Console.WriteLine("Kursas nebuvo ištrintas");
			}
		}
	}
}
