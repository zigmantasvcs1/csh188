﻿using CoursesApplication.Handlers;
using DataAccess.Repositories;

namespace CoursesApplication
{
	internal class Program
	{
		static async Task Main(string[] args)
		{
			Console.WriteLine("Hello, Database!");

			var dbContextFactory = new CoursesDbContextFactory();

			using var context = dbContextFactory.CreateDbContext(args);

			#region Setup
			CourseRepository courseRepository = new CourseRepository(context);
			CoursesHandler coursesHandler = new CoursesHandler(courseRepository);
			
			LecturerRepository lecturerRepository = new LecturerRepository(context);
			LecturersHandler lecturersHandler = new LecturersHandler(lecturerRepository);

			StudentRepository studentRepository = new StudentRepository(context);
			StudentsHandler studentsHandler = new StudentsHandler(studentRepository);

			OngoingCourseRepository ongoingCourseRepository = new OngoingCourseRepository(context);
			OngoingCoursesHandler ongoingCoursesHandler = new OngoingCoursesHandler(ongoingCourseRepository);

			OngoingCourseLineRepository ongoingCourseLineRepository = new OngoingCourseLineRepository(context);
			OngoingCourseLinesHandler ongoingCourseLinesHandler = new OngoingCourseLinesHandler(
				ongoingCourseLineRepository, 
				ongoingCourseRepository
			);

			ApplicationUI app = new ApplicationUI(
				coursesHandler,
				lecturersHandler,
				studentsHandler,
				ongoingCoursesHandler,
				ongoingCourseLinesHandler
			);
			#endregion

			await app.RunAsync();
		}
	}
}
