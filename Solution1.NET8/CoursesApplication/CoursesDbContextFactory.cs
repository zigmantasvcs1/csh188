﻿using DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoursesApplication
{
	public class CoursesDbContextFactory : IDesignTimeDbContextFactory<CoursesDbContext>
	{
		public CoursesDbContext CreateDbContext(string[] args)
		{
			// konfiguracijos uzkelimas is appsettings.json
			IConfigurationRoot configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json")
				.Build();

			// connection stirng pasiemimas
			string connectionString = configuration.GetConnectionString("DefaultConnection");

			var builder = new DbContextOptionsBuilder<CoursesDbContext>();
			builder.UseSqlServer(connectionString);

			return new CoursesDbContext(builder.Options);
		}
	}
}
