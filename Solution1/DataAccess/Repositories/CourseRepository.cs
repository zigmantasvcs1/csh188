﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
	public class CourseRepository // Sukuriama CourseRepository klasė, skirta valdyti kursų informaciją duomenų bazėje
	{
		private readonly CoursesDbContext _context; // Deklaruojamas privatus readonly laukas _context, kuris yra CoursesDbContext tipo. Jis naudojamas bendrauti su duomenų baze

		public CourseRepository(CoursesDbContext context) // Konstruktorius, kuris priima CoursesDbContext tipo objektą ir priskiria jį _context laukui
		{
			_context = context;
		}

		// CRUD operacijos - Create, Read, Update, Delete

		// Create
		public async Task CreateAsync(Course course) // Asinchroninis metodas CreateAsync, skirtas įrašyti naują kursą į duomenų bazę
		{
			_context
				.Courses
				.Add(course); // Pridedamas kursas į Courses kolekciją

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Read
		public async Task<Course?> ReadAsync(int id) // Asinchroninis metodas ReadAsync, skirtas gauti kursą pagal jo ID
		{
			return await _context
				.Courses
				.FirstOrDefaultAsync(x => x.Id == id); // Grąžinamas pirmas rastas kursas pagal ID arba null, jei tokio nėra
		}

		public async Task<List<Course>> ReadAsync() // Asinchroninis metodas ReadAsync, skirtas gauti visus kursus
		{
			return await _context.Courses.ToListAsync(); // Grąžinamas visų kursų sąrašas
		}

		// Update
		public async Task UpdateAsync(Course course) // Asinchroninis metodas UpdateAsync, skirtas atnaujinti esamo kurso informaciją
		{
			//_context.Update(course);
			var existingCourse = await _context
				.Courses
				.FindAsync(course.Id); // Ieškomas kursas pagal ID

			if (existingCourse == null) // Jei kursas nerastas, išmetama išimtis (Exception)
			{
				throw new KeyNotFoundException($"Course with ID {course.Id} not found.");
			}

			_context
				.Entry(existingCourse)
				.CurrentValues
				.SetValues(course); // Atnaujinamos esamo kurso reikšmės

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Delete
		public async Task DeleteAsync(int id) // Asinchroninis metodas DeleteAsync, skirtas ištrinti kursą pagal ID
		{
			var student = await _context
				.Courses
				.FindAsync(id); // Ieškomas kursas pagal ID

			if (student == null) // Jei kursas nerastas, išmetama išimtis
			{
				throw new KeyNotFoundException($"Course with ID {id} not found.");
			}

			_context
				.Courses
				.Remove(student); // Pašalinamas kursas iš Courses kolekcijos

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}
	}
}
