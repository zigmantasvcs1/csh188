﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
	public class StudentRepository // Sukuriama StudentRepository klasė, skirta valdyti studentų informaciją duomenų bazėje
	{
		private readonly CoursesDbContext _context; // Privatus readonly laukas _context, naudojamas bendrauti su duomenų baze

		public StudentRepository(CoursesDbContext context) // Konstruktorius, kuris priima CoursesDbContext tipo objektą ir priskiria jį _context laukui
		{
			_context = context;
		}

		// CRUD operacijos

		// Create
		public async Task CreateAsync(Student student) // Asinchroninis metodas CreateAsync, skirtas įrašyti naują studentą į duomenų bazę
		{
			_context.Students.Add(student); // Pridedamas studentas į Students kolekciją

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Read
		public async Task<Student?> ReadAsync(int id) // Asinchroninis metodas ReadAsync, skirtas gauti studentą pagal jo ID
		{
			return await _context.Students.FirstOrDefaultAsync(x => x.Id == id); // Grąžinamas pirmas rastas studentas pagal ID arba null, jei tokio nėra
		}

		public async Task<List<Student>> ReadAsync() // Asinchroninis metodas ReadAsync, skirtas gauti visus studentus
		{
			return await _context.Students.ToListAsync(); // Grąžinamas visų studentų sąrašas
		}

		// Update
		public async Task UpdateAsync(Student student) // Asinchroninis metodas UpdateAsync, skirtas atnaujinti esamo studento informaciją
		{
			var existingStudent = await _context.Students.FindAsync(student.Id); // Ieškomas studentas pagal ID

			if (existingStudent == null) // Jei studentas nerastas, išmetama išimtis
			{
				throw new KeyNotFoundException($"Student with ID {student.Id} not found.");
			}

			_context.Entry(existingStudent).CurrentValues.SetValues(student); // Atnaujinamos esamo studento reikšmės

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Delete
		public async Task DeleteAsync(int id) // Asinchroninis metodas DeleteAsync, skirtas ištrinti studentą pagal ID
		{
			var student = await _context.Students.FindAsync(id); // Ieškomas studentas pagal ID

			if (student == null) // Jei studentas nerastas, išmetama išimtis
			{
				throw new KeyNotFoundException($"Student with ID {id} not found.");
			}

			_context.Students.Remove(student); // Pašalinamas studentas iš Students kolekcijos

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}
	}
}
