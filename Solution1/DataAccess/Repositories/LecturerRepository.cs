﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repositories
{
	public class LecturerRepository // Sukuriama LecturerRepository klasė, skirta valdyti dėstytojų informaciją duomenų bazėje
	{
		private readonly CoursesDbContext _context; // Privatus readonly laukas _context, naudojamas bendrauti su duomenų baze

		public LecturerRepository(CoursesDbContext context) // Konstruktorius, kuris priima CoursesDbContext tipo objektą ir priskiria jį _context laukui
		{
			_context = context;
		}

		// CRUD operacijos

		// Create
		public async Task CreateAsync(Lecturer lecturer) // Asinchroninis metodas CreateAsync, skirtas įrašyti naują dėstytoją į duomenų bazę
		{
			_context.Lecturers.Add(lecturer); // Pridedamas dėstytojas į Lecturers kolekciją

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Read
		public async Task<Lecturer?> ReadAsync(int id) // Asinchroninis metodas ReadAsync, skirtas gauti dėstytoją pagal jo ID
		{
			return await _context
				.Lecturers
				.FirstOrDefaultAsync(x => x.Id == id); // Grąžinamas pirmas rastas dėstytojas pagal ID arba null, jei tokio nėra
		}

		public async Task<List<Lecturer>> ReadAsync() // Asinchroninis metodas ReadAsync, skirtas gauti visus dėstytojus
		{
			return await _context.Lecturers.ToListAsync(); // Grąžinamas visų dėstytojų sąrašas
		}

		// Update
		public async Task UpdateAsync(Lecturer lecturer) // Asinchroninis metodas UpdateAsync, skirtas atnaujinti esamo dėstytojo informaciją
		{
			var existingLecturer = await _context
				.Lecturers
				.FindAsync(lecturer.Id); // Ieškomas dėstytojas pagal ID

			if (existingLecturer == null) // Jei dėstytojas nerastas, išmetama išimtis
			{
				throw new KeyNotFoundException($"Lecturer with ID {lecturer.Id} not found.");
			}

			_context
				.Entry(existingLecturer)
				.CurrentValues
				.SetValues(lecturer); // Atnaujinamos esamo dėstytojo reikšmės

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}

		// Delete
		public async Task DeleteAsync(int id) // Asinchroninis metodas DeleteAsync, skirtas ištrinti dėstytoją pagal ID
		{
			var lecturer = await _context
				.Lecturers
				.FindAsync(id); // Ieškomas dėstytojas pagal ID

			if (lecturer == null) // Jei dėstytojas nerastas, išmetama išimtis
			{
				throw new KeyNotFoundException($"Lecturer with ID {id} not found.");
			}

			_context
				.Lecturers
				.Remove(lecturer); // Pašalinamas dėstytojas iš Lecturers kolekcijos

			await _context.SaveChangesAsync(); // Išsaugomi pakeitimai duomenų bazėje asinchroniškai
		}
	}
}
