﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
	public class CoursesDbContext : DbContext
	{
		public CoursesDbContext(DbContextOptions options) : base(options)
		{
		}

		public DbSet<Course> Courses { get; set; }
		public DbSet<Lecturer> Lecturers { get; set; }
		public DbSet<Student> Students { get; set; }
		public DbSet<OngoingCourse> OngoingCourses { get; set; }
		public DbSet<OngoingCourseLine> OngoingCourseLines { get; set; }



	}
}
