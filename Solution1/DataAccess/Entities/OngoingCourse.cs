﻿namespace DataAccess.Entities
{
	public class OngoingCourse : BaseEntity
	{
		private OngoingCourse() { }

		public OngoingCourse(
			int courseId,
			DateTime startDate,
			DateTime? endDate,
			int id,
			DateTime createdAt) 
			: base(
				  id,
				  createdAt)
		{
			CourseId = courseId;
			StartDate = startDate;
			EndDate = endDate;
		}

		public int CourseId { get; private set; }
		public Course Course { get; private set; } = null!; // navigational property
		public DateTime StartDate { get; private set; }
		public DateTime? EndDate { get; private set; }
	}
}
