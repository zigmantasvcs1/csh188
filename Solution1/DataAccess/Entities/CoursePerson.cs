﻿using System.ComponentModel.DataAnnotations;

namespace DataAccess.Entities
{
	public class CoursePerson : BaseEntity
	{
		public CoursePerson(
			string name,
			string surname,
			string email,
			string documentNumber,
			int id, 
			DateTime createdAt) 
			: base(
				  id, 
				  createdAt
			)
		{
			Name = name;
			Surname = surname;
			Email = email;
			DocumentNumber = documentNumber;
		}

		[MaxLength(50)]
		public string Name { get; private set; } = null!;

		[MaxLength(50)]
		public string Surname { get; private set; } = null!;

		[MaxLength(100)]
		public string Email { get; private set; } = null!;

		[MaxLength(6)]
		public string DocumentNumber { get; private set; } = null!;
	}
}
