﻿
namespace DataAccess.Entities
{
	public class Lecturer : CoursePerson
	{
		public Lecturer(
			string name,
			string surname,
			string email,
			string documentNumber,
			int id,
			DateTime createdAt) 
			: base(
				  name,
				  surname,
				  email,
				  documentNumber,
				  id,
				  createdAt)
		{
		}
	}
}
