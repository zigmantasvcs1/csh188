﻿using DataAccess.Entities;
using DataAccess.Repositories;
using Newtonsoft.Json;

namespace CoursesApplication.Handlers
{
	public class OngoingCourseLinesHandler
	{
		private readonly OngoingCourseLineRepository _ongoingCourseLineRepository;
		private readonly OngoingCourseRepository _ongoingCourseRepository;

		public OngoingCourseLinesHandler(
			OngoingCourseLineRepository ongoingCourseLineRepository, 
			OngoingCourseRepository ongoingCourseRepository)
		{
			_ongoingCourseLineRepository = ongoingCourseLineRepository;
			_ongoingCourseRepository = ongoingCourseRepository;
		}

		public async Task HandleAsync()
		{
			var continueKey = "t";
			do
			{
				Console.WriteLine("Patekote į vykstančių kursų eilučių aplinką");
				Console.WriteLine("Ką norite daryti");
				Console.WriteLine("1 - eksportuoti vykstančių kursų eilutes į JSON failą");
				Console.WriteLine("2 - išspausdinti vykstančių kursu ekrane");
				Console.WriteLine("3 - išspausdinti vykstančio kurso eilutes ekrane");
				Console.WriteLine("4 - sukurti vykstančių kursų eilutę");
				Console.WriteLine("5 - pakeisti vykstančių kursų eilutę");
				Console.WriteLine("6 - ištrinti vykstančių kursų eilutę");

				Console.WriteLine("Įveskite ką renkatės");
				switch (Console.ReadLine())
				{
					case "1":
						await ExportAsync();
						break;
					case "2":
						await PrintAsync();
						break;
					case "3":
						await PrintDetailsAsync();
						break;
					case "4":
						await CreateAsync();
						break;
					case "5":
						await ChangeAsync();
						break;
					case "6":
						await DeleteAsync();
						break;
					default:
						Console.WriteLine("Nėra tokio pasirinkimo");
						break;
				}

				Console.WriteLine("Ar tęsiame? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}

		private async Task ExportAsync()
		{
			List<OngoingCourseLine> ongoingCourseLines = await _ongoingCourseLineRepository.ReadAsync();

			var jsString = JsonConvert.SerializeObject(ongoingCourseLines);

			ExportToJson(jsString, "ongoingCourseLines.json");
			Console.WriteLine("Failas ongoingCourseLines.json exportuotas");
		}

		private void ExportToJson(string json, string fileName)
		{
			using (var streamWriter = new StreamWriter(fileName))
			{
				streamWriter.WriteLine(json);
			}
		}

		private async Task PrintAsync()
		{
			List<OngoingCourse> ongoingCourses = await _ongoingCourseRepository.ReadAsync();

			Console.WriteLine("Vykstantys kursai:");
			Console.WriteLine($"" +
				$"{"Id",-5} " +
				$"{"Antraštė",-20} " +
				$"{"Pradžia",-20} " +
				$"{"Pabaiga",-25} " +
				$"{"Sukurta",-25}"
			);

			foreach (var ongoingCourse in ongoingCourses)
			{
				Console.WriteLine($"" +
					$"{ongoingCourse.Id,-5} " +
					$"{ongoingCourse.Course?.Title,-20} " +
					$"{ongoingCourse.StartDate:yyyy-MM-dd,-20} " +
					$"{ResolveDateTimeString(ongoingCourse),-25} " +
					$"{ongoingCourse.CreatedAt:yyyy-MM-dd,-15}");
			}
		}

		private string ResolveDateTimeString(OngoingCourse ongoingCourse)
		{
			return ongoingCourse.EndDate.HasValue
				? ongoingCourse.EndDate.Value.ToString("yyyy-MM-dd")
				: string.Empty;
		}

		private async Task PrintDetailsAsync()
		{
			Console.WriteLine("Iveskite vysktancio kurso id");
			int id = int.Parse(Console.ReadLine());

			List<OngoingCourseLine> ongoingCourseLines = await _ongoingCourseLineRepository.ReadAsync();
			List<OngoingCourseLine> filteredOngoingCourseLines = ongoingCourseLines
				.Where(x => x.OngoingCourseId == id)
				.ToList();

			List<OngoingCourse> ongoingCourses = await _ongoingCourseRepository.ReadAsync();

			string courseTitle = filteredOngoingCourseLines.Any()
				? filteredOngoingCourseLines
					.First()
					.OngoingCourse
					.Course
					.Title
				: string.Empty;

			Console.WriteLine($"Vykstančio kurso {courseTitle} eilutės:");
			Console.WriteLine($"" +
				$"{"Id",-5} " +
				$"{"Vykstančio kurso id",-25} " +
				$"{"Studento id",-25} " +
				$"{"Lektoriaus id",-25}"
			);

			foreach (var filteredOngoingCourseLine in filteredOngoingCourseLines)
			{
				Console.WriteLine($"" +
					$"{filteredOngoingCourseLine.Id,-5} " +
					$"{filteredOngoingCourseLine.OngoingCourseId,-20} " +
					$"{filteredOngoingCourseLine.LecturerId} " +
					$"{filteredOngoingCourseLine.StudentId}"
				);
			}
		}

		private async Task CreateAsync()
		{
			Console.WriteLine("Suveskite vykstančio kurso eilutės duomenis");

			Console.WriteLine("Įveskite vysktančio kurso id ");
			int ongoingCourseId = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite lektoriaus id ");
			int lecturerId = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite studento id ");
			int studentId = int.Parse(Console.ReadLine());

			OngoingCourseLine ongoingCourseLine = new OngoingCourseLine(
				ongoingCourseId,
				lecturerId,
				studentId,
				0,
				DateTime.MinValue
			);

			await _ongoingCourseLineRepository.CreateAsync(ongoingCourseLine);

			Console.WriteLine("Vykstančio kurso eilutė sukurta");
		}

		private async Task ChangeAsync()
		{
			Console.WriteLine("Suveskite vykstančio kurso eilutės duomenis, kuriuos keisite");

			Console.WriteLine("Kokį vyktantį kursą keisite? Pasirinkite id iš sąrašo. Jums reikės kurso eilutės id");
			await PrintDetailsAsync();

			Console.WriteLine("Įveskite kurso eilutės id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite vykstančio kurso id į kurį keičiate");
			int ongoingCourseId = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite lektoriaus id į kurį keičiate");
			int lecturerId = int.Parse(Console.ReadLine());

			Console.WriteLine("Įveskite studento id į kurį keičiate");
			int studentId = int.Parse(Console.ReadLine());

			OngoingCourseLine ongoingCourseLine = new OngoingCourseLine(
				ongoingCourseId,
				lecturerId,
				studentId,
				id,
				DateTime.MinValue
			);

			await _ongoingCourseLineRepository.UpdateAsync(ongoingCourseLine);

			Console.WriteLine("Vykstančio kurso eilutė koreguota");
		}

		private async Task DeleteAsync()
		{
			Console.WriteLine("Kokį vykstančio kurso eilutę trinsite?");

			await PrintDetailsAsync();

			Console.WriteLine("Įveskite vykstančio kurso eilutės id");
			int id = int.Parse(Console.ReadLine());

			Console.WriteLine("Ar tikrai to norite? t/n");

			if (Console.ReadLine() == "t")
			{
				await _ongoingCourseRepository.DeleteAsync(id);

				Console.WriteLine($"Vykstančio kurso eilutė ištrinta");
			}
			else
			{
				Console.WriteLine("Vykstančio kurso eilutė nebuvo ištrinta");
			}
		}
	}
}
