﻿using CoursesApplication.Handlers;

namespace CoursesApplication
{
	public class ApplicationUI
	{
		private readonly CoursesHandler _coursesHandler;
		private readonly LecturersHandler _lecturersHandler;
		private readonly StudentsHandler _studentsHandler;
		private readonly OngoingCoursesHandler _ongoingCoursesHandler;
		private readonly OngoingCourseLinesHandler _ongoingCourseLinesHandler;

		public ApplicationUI(
			CoursesHandler coursesHandler,
			LecturersHandler lecturersHandler,
			StudentsHandler studentsHandler,
			OngoingCoursesHandler ongoingCoursesHandler,
			OngoingCourseLinesHandler ongoingCourseLinesHandler)
		{
			_coursesHandler = coursesHandler;
			_lecturersHandler = lecturersHandler;
			_studentsHandler = studentsHandler;
			_ongoingCoursesHandler = ongoingCoursesHandler;
			_ongoingCourseLinesHandler = ongoingCourseLinesHandler;
		}

		public async Task RunAsync()
		{
			string continueKey = "t";

			do
			{
				Console.WriteLine("Sveiki! Ką norite veikti?");
				Console.WriteLine("1 - kurso valdymas");
				Console.WriteLine("2 - lektorių valdymas");
				Console.WriteLine("3 - studentų valdymas");
				Console.WriteLine("4 - vykstančių kursų valdymas");
				Console.WriteLine("5 - vykstančių kursų eilučių valdymas");

				try
				{
					Console.WriteLine("Įveskite ką renkatės");
					switch (Console.ReadLine())
					{
						case "1":
							await _coursesHandler.HandleAsync();
							break;
						case "2":
							await _lecturersHandler.HandleAsync();
							break;
						case "3":
							await _studentsHandler.HandleAsync();
							break;
						case "4":
							await _ongoingCoursesHandler.HandleAsync();
							break;
						case "5":
							await _ongoingCourseLinesHandler.HandleAsync();
							break;
						default:
							Console.WriteLine("Nėra tokio pasirinkimo");
							break;
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("Įvyko bėda. Klauskite administratoriaus.");
				}

				Console.WriteLine("Esate pagrindineje dalyje. Ar bandom iš naujo? (t/n)");
				continueKey = Console.ReadLine();
			} while (continueKey == "t");
		}
	}
}
